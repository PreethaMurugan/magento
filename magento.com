# Magento Vars
# set $MAGE_ROOT /path/to/magento/root;
# set $MAGE_MODE default; # or production or developer

upstream fastcgi_backend {
       server   unix:/var/run/php/php7.4-fpm.sock;
}

server {
        listen 80;
        server_name local.magento.com;
	set $MAGE_ROOT /home/ziffity/magento;
#	root /home/ziffity/magento;
	include /home/ziffity/magento/nginx.conf.sample;
        fastcgi_read_timeout 300;
        fastcgi_buffers 8 128k;
        fastcgi_buffer_size 256k;
}
